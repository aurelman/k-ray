package io.androweed.kray.core

import kotlin.math.max

data class Color(
    val red: Float = 0.0f,
    val green: Float = 0.0f,
    val blue: Float = 0.0f
) {
    companion object {
        private const val COMPONENT_MIN_VALUE = 0.0f
        private const val COMPONENT_MAX_VALUE = 1.0f
        private val VALID_RANGE = (COMPONENT_MIN_VALUE..COMPONENT_MAX_VALUE)

        private fun allComponentsValid(red: Float, green: Float, blue: Float) =
            VALID_RANGE.contains(red) && VALID_RANGE.contains(green) && VALID_RANGE.contains(blue)

        private fun clamped(rawRed: Float, rawGreen: Float, rawBlue: Float): Color {
            val max = max(rawRed, max(rawGreen, rawBlue))
            if (max <= COMPONENT_MAX_VALUE) {
                return Color(rawRed, rawGreen, rawBlue)
            }
            return Color(rawRed / max, rawGreen / max, rawBlue / max)
        }
    }

    init {
        require(allComponentsValid(red, green, blue)) {
            "All color components should be within [$COMPONENT_MIN_VALUE-$COMPONENT_MAX_VALUE] " +
                "but were [red: $red, green: $green, blue: $blue]"
        }
    }

    operator fun plus(color: Color) =
        clamped(red + color.red, green + color.green, blue + color.blue)

    operator fun times(scalar: Number) =
        clamped(red * scalar.toFloat(), green * scalar.toFloat(), blue * scalar.toFloat())
}

operator fun Number.times(color: Color) = color * this