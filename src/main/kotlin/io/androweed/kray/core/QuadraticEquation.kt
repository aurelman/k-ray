package io.androweed.kray.core

import kotlin.math.sign
import kotlin.math.sqrt

/**
 * Solves quadratic equations.
 * This class has value semantic, so that is can be considered immutable and thread safe.
 *
 * Note that for performance matters the result of some intermediate computations can be cached.
 */
data class QuadraticEquation(
    val a: Double,
    val b: Double,
    val c: Double
) {
    fun solve(): Array<Double> {
        return solutions
    }

    private val solutions: Array<Double> by lazy {
        if (a == 0.0) {
            return@lazy arrayOf(-b / c)
        }

        if (determinant < 0.0) {
            return@lazy emptyArray<Double>()
        }

        if (determinant == 0.0) {
            return@lazy arrayOf(-b / (2 * a))
        }

        val rootSquaredDelta = sqrt(determinant)

        /*
         *  The numerically computed solution would be  :
         *   double root1 = (-b - rootSquaredDelta)/denominator;
         *   double root2 = (-b + rootSquaredDelta)/denominator;
         *
         *  But to avoid cancellation issues in case b*b is much greater that 4*a*c
         *  we compute roots in an other way.
         *
         *  For more details about cancellation see : https://en.wikipedia.org/wiki/Loss_of_significance
         */
        val root1 = (-b - sign(b) * rootSquaredDelta) / (2 * a)
        val root2 = c / (a * root1)

        return@lazy if (root1 < root2) {
            arrayOf(root1, root2)
        } else {
            arrayOf(root2, root1)
        }
    }

    private val determinant by lazy {
        b * b - 4 * a * c
    }
}