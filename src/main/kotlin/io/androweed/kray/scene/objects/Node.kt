package io.androweed.kray.io.androweed.kray.scene.objects

import io.androweed.kray.core.Vector

class Node(val position: Vector)