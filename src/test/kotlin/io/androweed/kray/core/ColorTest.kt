package io.androweed.kray.core

import assertk.all
import assertk.assertThat
import assertk.assertions.hasClass
import assertk.assertions.hasMessage
import assertk.assertions.isCloseTo
import assertk.assertions.isFailure
import assertk.assertions.isSuccess
import org.junit.jupiter.api.Test

internal class ColorTest {

    @Test
    fun`it should not raise any errors when its components are within 0-1 range`() {
        assertThat {
            Color(0.0f, 1.0f, 0.0f)
        }.isSuccess()
    }

    @Test
    fun `it should raise error if any of its components are negative`() {
        assertThat {
            Color(-1.0f, -1.0f, -1.0f)
        }.isFailure().all {
            hasClass(IllegalArgumentException::class)
            hasMessage("All color components should be within [0.0-1.0] but were [red: -1.0, green: -1.0, blue: -1.0]")
        }
    }

    @Test
    fun `should raise error if any of the component is greater than 1`() {
        assertThat {
            Color(2.0f, 2.0f, 2.0f)
        }.isFailure().all {
            hasClass(IllegalArgumentException::class)
            hasMessage("All color components should be within [0.0-1.0] but were [red: 2.0, green: 2.0, blue: 2.0]")
        }
    }

    @Test
    fun `should return the resulting vector when added to another vector`() {
        val result = Color(0.5f, 0.5f, 0.5f) + Color(0.3f, 0.3f, 0.3f)

        assertThat(result.red).isCloseTo(0.8f, DELTA)
        assertThat(result.green).isCloseTo(0.8f, DELTA)
        assertThat(result.blue).isCloseTo(0.8f, DELTA)
    }

    @Test
    fun `it should clamp components by dividing resulting components by the max one  when added to another vector`() {
        val result = Color(0.2f, 1.0f, 1.0f) + Color(0.3f, 0.0f, 1.0f)

        assertThat(result.red).isCloseTo(0.25f, DELTA)
        assertThat(result.green).isCloseTo(0.5f, DELTA)
        assertThat(result.blue).isCloseTo(1f, DELTA)
    }

    @Test
    fun `it should return the resulting vector when multiplied by a scalar value`() {
        val result = Color(0.4f, 0.4f, 0.4f) * 0.5

        assertThat(result.red).isCloseTo(0.2f, DELTA)
        assertThat(result.green).isCloseTo(0.2f, DELTA)
        assertThat(result.blue).isCloseTo(0.2f, DELTA)
    }

    @Test
    fun `it should clamp components values when multiplying by a scalar would lead to component higher than 1`() {
        val result = Color(1.0f, 0.5f, 0.2f) * 2.0

        assertThat(result.red).isCloseTo(1f, DELTA)
        assertThat(result.green).isCloseTo(0.5f, DELTA)
        assertThat(result.blue).isCloseTo(0.2f, DELTA)
    }

    @Test
    fun `it should return the resulting vector when multiplied by a scalar value left`() {
        val result = 0.5 * Color(0.4f, 0.4f, 0.4f)

        assertThat(result.red).isCloseTo(0.2f, DELTA)
        assertThat(result.green).isCloseTo(0.2f, DELTA)
        assertThat(result.blue).isCloseTo(0.2f, DELTA)
    }

    @Test
    fun `it should clamp components when multiplying left by a scalar that would lead to component higher than 1`() {
        val result = 2.0 * Color(1.0f, 0.5f, 0.2f)

        assertThat(result.red).isCloseTo(1f, DELTA)
        assertThat(result.green).isCloseTo(0.5f, DELTA)
        assertThat(result.blue).isCloseTo(0.2f, DELTA)
    }

    companion object {
        private const val DELTA = 0.0000001f
    }
}