package io.androweed.kray.core

import assertk.assertThat
import assertk.assertions.hasSize
import assertk.assertions.isCloseTo
import assertk.assertions.isEmpty
import org.junit.jupiter.api.Test

internal class QuadraticEquationTest {

    @Test
    fun `it should return nothing if no solutions exist`() {
        val equ = QuadraticEquation(2.0, -1.0, 2.0)
        assertThat(equ.solve()).isEmpty()
    }

    @Test
    fun `it should return the only solution`() {
        val equ = QuadraticEquation(0.0, 5.0, 2.0)

        val solutions = equ.solve()

        assertThat(solutions).hasSize(1)
        assertThat(solutions[0]).isCloseTo(-5.0 / 2, DELTA)
    }

    @Test
    fun `it should return the only solution with a != 0`() {
        val equ = QuadraticEquation(1.0, 2.0, 1.0)

        val solutions = equ.solve()

        assertThat(solutions).hasSize(1)
        assertThat(solutions[0]).isCloseTo(-1.0, DELTA)
    }

    @Test
    fun `it should return return both possible solutions`() {
        val equ = QuadraticEquation(2.0, 5.0, 2.0)

        val solutions = equ.solve()

        assertThat(solutions).hasSize(2)

        assertThat(solutions[0]).isCloseTo(-2.0, DELTA)
        assertThat(solutions[1]).isCloseTo(-0.5, DELTA)
    }

    @Test
    fun `it should avoid cancellation issues`() {
        val equWithPossibleCancellation = QuadraticEquation(1.0, 200.0, -0.000015)

        val solutions = equWithPossibleCancellation.solve()

        assertThat(solutions).hasSize(2)
        assertThat(solutions[0]).isCloseTo(-200.000000075, 0.000000000001)
        assertThat(solutions[1]).isCloseTo(0.000000075, 0.000000000001)
    }

    companion object {
        private const val DELTA = 0.0000001
    }
}