package io.androweed.kray.core

import assertk.assertThat
import assertk.assertions.isEqualTo
import io.androweed.kray.io.androweed.kray.scene.objects.IntersectionResult
import io.androweed.kray.io.androweed.kray.scene.objects.Sphere
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

internal class RayTest {

    @Test
    fun `it should delegate intersection computation to primitive`() {
        val ray = Ray(origin = Vector(0.0, 0.0, 0.0), direction = Vector(0.0, 0.0, 1.0))

        // Have to mock a concrete class because mocking an interface [Primitive] will lead to
        // an exception telling it can't instantiate the object.
        val primitive = mockk<Sphere>()
        val intersection = IntersectionResult(intersect = false, intersection = null, primitive = primitive)

        every { primitive.intersect(ray) } returns intersection

        val result = ray.intersect(primitive)

        assertThat(result).isEqualTo(intersection)
        verify { primitive.intersect(ray) }
    }
}