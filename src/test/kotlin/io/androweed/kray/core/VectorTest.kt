package io.androweed.kray.core

import assertk.assertThat
import assertk.assertions.isCloseTo
import assertk.assertions.isEqualTo
import assertk.assertions.isSameAs
import org.junit.jupiter.api.Test

internal class VectorTest {

    @Test
    fun `it should returns its components`() {
        val vec = Vector(1.0, 5.0, -12.0)

        assertThat(vec.x).isEqualTo(1.0)
        assertThat(vec.y).isEqualTo(5.0)
        assertThat(vec.z).isEqualTo(-12.0)
    }

    @Test
    fun `it should return the result of an addition between 2 vectors`() {
        val vec = Vector(1.0, 5.0, -12.0)
        val vec2 = Vector(2.0, 3.0, 5.0)

        val vec3 = vec + vec2

        assertThat(vec3.x).isCloseTo(3.0, DELTA)
        assertThat(vec3.y).isCloseTo(8.0, DELTA)
        assertThat(vec3.z).isCloseTo(-7.0, DELTA)
    }

    @Test
    fun `it should return the result of a subtraction between two vectors`() {
        val vec = Vector(1.0, 5.0, -12.0)
        val vec2 = Vector(2.0, 3.0, 5.0)

        val vec3 = vec - vec2

        assertThat(vec3.x).isCloseTo(-1.0, DELTA)
        assertThat(vec3.y).isCloseTo(2.0, DELTA)
        assertThat(vec3.z).isCloseTo(-17.0, DELTA)
    }
    @Test
    fun `it should return the result of the product by a scalar`() {
        val vec = Vector(1.0, 5.0, -12.0)

        val vec3 = vec * 2.0

        assertThat(vec3.x).isCloseTo(2.0, DELTA)
        assertThat(vec3.y).isCloseTo(10.0, DELTA)
        assertThat(vec3.z).isCloseTo(-24.0, DELTA)
    }

    @Test
    fun `product by a scalar should be a commutative operation`() {
        val vec = Vector(1.0, 5.0, -12.0)

        val vec3 = 2.0 * vec

        assertThat(vec3.x).isCloseTo(2.0, DELTA)
        assertThat(vec3.y).isCloseTo(10.0, DELTA)
        assertThat(vec3.z).isCloseTo(-24.0, DELTA)
    }

    @Test
    fun `it should return the opposite vector`() {
        val vec = Vector(1.0, 5.0, -12.0)

        val vec2 = -vec

        assertThat(vec2.x).isCloseTo(-1.0, DELTA)
        assertThat(vec2.y).isCloseTo(-5.0, DELTA)
        assertThat(vec2.z).isCloseTo(12.0, DELTA)
    }

    @Test
    fun `it should return the magnitude of the vector`() {
        val vec = Vector(1.0, 5.0, -12.0)

        assertThat(vec.length).isCloseTo(13.0384048, DELTA)
    }

    @Test
    fun `it should return the squared magnitude of the vector`() {
        val vec = Vector(1.0, 5.0, -12.0)

        assertThat(vec.squaredLength).isCloseTo(170.0, DELTA)
    }

    @Test
    fun `it should compute the result of the dot product with another vector`() {
        val vec = Vector(1.0, 5.0, -12.0)
        val other = Vector(-5.0, 8.0, 25.0)

        assertThat(vec dot other).isCloseTo(-265.0, DELTA)
    }

    @Test
    fun `it should compute the result of the  cross product with another vector`() {
        val vec = Vector(1.0, 5.0, -12.0)
        val other = Vector(-5.0, 8.0, 25.0)

        val result = vec cross other

        assertThat(result.x).isCloseTo(221.0, DELTA)
        assertThat(result.y).isCloseTo(35.0, DELTA)
        assertThat(result.z).isCloseTo(33.0, DELTA)
    }

    @Test
    fun `it should properly normalize a vector`() {
        val vec = Vector(1.0, 5.0, -12.0)
        val result = vec.normalized()

        assertThat(vec.normalized().length).isCloseTo(1.0, DELTA)

        assertThat(result.x).isCloseTo(0.0766964, DELTA)
        assertThat(result.y).isCloseTo(0.3834824, DELTA)
        assertThat(result.z).isCloseTo(-0.9203579, DELTA)
    }

    @Test
    fun `it should return a vector joining two positions`() {
        val origin = Position(1.0, 1.0, 1.0)
        val target = Position(10.0, 10.0, 10.0)
        val result = origin toward target

        assertThat(result.x).isCloseTo(9.0, DELTA)
        assertThat(result.y).isCloseTo(9.0, DELTA)
        assertThat(result.z).isCloseTo(9.0, DELTA)
    }

    @Test
    fun `it should return the same instance if calling normalized on an already normalized instance`() {
        val normalized = Vector(1.0, 1.0, 1.0).normalized()

        assertThat(normalized.normalized()).isSameAs(normalized)
    }

    @Test
    fun `it should offer some convenient constant default instances`() {

        assertThat(Vector.ZERO).isEqualTo(Vector(0.0, 0.0, 0.0))
        assertThat(Vector.X_UNIT).isEqualTo(Vector(1.0, 0.0, 0.0))
        assertThat(Vector.Y_UNIT).isEqualTo(Vector(0.0, 1.0, 0.0))
        assertThat(Vector.Z_UNIT).isEqualTo(Vector(0.0, 0.0, 1.0))
    }

    companion object {
        private const val DELTA = 0.000001
    }
}