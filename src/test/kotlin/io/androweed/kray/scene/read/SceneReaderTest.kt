package io.androweed.kray.scene.read

import assertk.all
import assertk.assertThat
import assertk.assertions.hasClass
import assertk.assertions.hasMessage
import assertk.assertions.isFailure
import org.junit.jupiter.api.Test

internal class SceneReaderTest {

    @Test
    fun `it should raise an exception if Json is malformed`() {
        val sceneReader = SceneReader

        assertThat {
            sceneReader.read("{[}")
        }.isFailure().all {
            hasClass(SceneReadingException::class)
        }
    }

    @Test
    fun `it should raise an exception when no camera is defined`() {
        val sceneReader = SceneReader

        assertThat {
            sceneReader.read("""{  }""")
        }.isFailure().all {
            hasClass(SceneReadingException::class)
            hasMessage("missing attribute 'camera'")
        }
    }
}