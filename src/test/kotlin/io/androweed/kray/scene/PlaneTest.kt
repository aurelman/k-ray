package io.androweed.kray.scene

import assertk.assertThat
import assertk.assertions.isEqualTo
import io.androweed.kray.core.Position
import io.androweed.kray.core.Vector
import io.androweed.kray.io.androweed.kray.scene.objects.Plane
import org.junit.jupiter.api.Test

internal class PlaneTest {

    @Test
    fun `it should return the normal at any point`() {
        val vector = Vector(5.0, 10.0, 30.0)
        val plane = Plane(Position(), vector)

        assertThat(plane.normalAt(Position())).isEqualTo(vector.normalized())
    }
}