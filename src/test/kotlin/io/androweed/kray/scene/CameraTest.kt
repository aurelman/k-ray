package io.androweed.kray.scene

import assertk.assertThat
import assertk.assertions.isEqualTo
import io.androweed.kray.core.Vector
import io.androweed.kray.io.androweed.kray.scene.objects.Camera
import org.junit.jupiter.api.Test

internal class CameraTest {

    @Test
    fun `it should normalize components`() {
        val cam = Camera(
            up = Vector(0.0, 2.0, 0.0),
            direction = Vector(0.0, 0.0, 2.0)
        )

        assertThat(cam.up).isEqualTo(Vector(0.0, 1.0, 0.0))
        assertThat(cam.direction).isEqualTo(Vector(0.0, 0.0, 1.0))
    }

    @Test
    fun `it should have a consistent right component computed`() {
        val cam = Camera(
            up = Vector(0.0, 2.0, 0.0),
            direction = Vector(0.0, 0.0, 2.0)
        )

        assertThat(cam.right).isEqualTo(Vector(1.0, 0.0, 0.0))
    }
}