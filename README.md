# k-ray
[![coverage report](https://gitlab.com/aurelman/k-ray/badges/master/coverage.svg)](https://gitlab.com/aurelman/k-ray/commits/master)

k-ray is a ray tracer implemented in kotlin.
Basically it is the rewrite of [ray-monde](https://github.com/aurelman/ray-monde) ray tracer initially written in Java.

One of the k-ray's main goal is to be executable in a concurrent/parallel mode but by keeping its codebase
as simple as possible. To accomplish this goal, k-ray makes heavy use of immutable classes and especially kotlin data classes 
 (those are a kotlin type of classes for value semantic objects) for its core objects (Vector, Color, Primitive...)
Immutable classes are by nature concurrent friendly and thread-safe, and do not need specific synchronization structure
to keep them consistent between every thread they go through.
 
