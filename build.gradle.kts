import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("idea")
    id("eclipse")

    kotlin("jvm") version "1.3.72"

    // A ktlin plugin for gradle
    id("org.jmailen.kotlinter") version "2.3.2"
    jacoco
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.5")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.10.+")
    implementation("io.github.microutils:kotlin-logging:1.7.9")

    testImplementation("org.junit.jupiter:junit-jupiter:5.6.+")
    testImplementation("io.mockk:mockk:1.9")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.21")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "12"
    }
}

/*
kotlinter {
// Put here ktlinter config if default one is not enough
}
*/